//Adam Atamnia 2036819

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestSubtractonacciSequence {
    @Test
    public void getTermTest1(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(2, fs.getTerm(0));
    }

    @Test
    public void getTermTest2(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(4, fs.getTerm(1));
    }

    @Test
    public void getTermTest3(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(-2, fs.getTerm(2));
    }

    @Test
    public void getTermTest4(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(6, fs.getTerm(3));
    }

    @Test
    public void getTermTest5(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(-8, fs.getTerm(4));
    }

    @Test
    public void getTermTest6(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(14, fs.getTerm(5));
    }

    @Test
    public void getTermTest7(){
        Sequence fs = new SubtractonacciSequence(2,4);
        assertEquals(-22, fs.getTerm(6));
    }

    @Test
    public void getTermExceptionTest(){
        Sequence fs = new SubtractonacciSequence(2,4);
        try {
            fs.getTerm(-1);
            fail("Test failed because this method should throw an exception.");
        } catch (IllegalArgumentException e) {
            
        }
        
    }
}