//Adam Atamnia 2036819

public class FibonacciSequence extends Sequence{
    private int num1;
    private int num2;


    public FibonacciSequence(int num1, int num2){
        this.num1 = num1;
        this.num2 = num2;
    }

    //do validation
    public int getTerm(int n){
        if (n < 0) {
            throw new IllegalArgumentException("Argument cannot be less than zero.");
        }
        int[] sequence = new int[n+1];
        sequence[0] = this.num1;

        if (n >= 1){
            sequence[1] = this.num2;
            if(n >= 2){
                for (int i = 2; i < sequence.length; i++) {
                    sequence[i] = sequence[i-1] + sequence[i-2];
                }
            }
        }
        return sequence[n];
    }
}