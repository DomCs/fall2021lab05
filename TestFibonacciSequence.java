//Domenico Cuscuna 2038364

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class TestFibonacciSequence {
    
    @Test
    //Testing the first term of the sequence
    public void testFirstTerm(){ 
        
        Sequence testOne = new FibonacciSequence(2,5);

        assertEquals(2, testOne.getTerm(0));
    }

    @Test
    //Testing the second term of the sequence
    public void testSecondTerm(){ 
        
        Sequence testTwo = new FibonacciSequence(2,5);

        assertEquals(5, testTwo.getTerm(1));
    }


    @Test
    //Testing a random term in the sequence
    public void testRandomTerm(){ 
        
        Sequence testThree = new FibonacciSequence(2,4);

        assertEquals(26, testThree.getTerm(5));
    }

    @Test
    //Testing another random term in the sequence
    public void testAnotherRandomTerm(){ 
        
        Sequence testThree = new FibonacciSequence(2,4);

        assertEquals(42, testThree.getTerm(6));
    }

    @Test
    //Testing to catch Exception

    public void testIllegalException(){
        Sequence testFour = new FibonacciSequence(2,4);

        try{
            testFour.getTerm(-1);
            fail("Method needs to throw an exception");
        }  
        catch(IllegalArgumentException e){
        }    
    }

}
