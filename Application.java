//Domenico Cuscuna 2038364 Adam Atamnia 2036819

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        String str = read.next();
        print(parse(str), 10);
        //"Fib;2;4;x;Sub;2;4;x;Fib;1;3;x"
    }


    //Print the first nth numbers from any sequences
    public static void print(Sequence[] seq, int n){

        for (int i = 0; i<seq.length; i++){
            
            System.out.println("Sequence "+(i+1)+": Printing "+n+" numbers");

            for(int j=0;j<n; j++){
                System.out.print(seq[i].getTerm(j)+"  ");
            }

            System.out.println();
            System.out.println();
        }

    }

    public static Sequence[] parse(String input){
        String[] strArray = input.split(";");
        int numbSequences = 0;
        for (String string : strArray) {
            if (string.equals("x")){
                numbSequences++;
            }
        }
        Sequence[] sqc = new Sequence[numbSequences];
        int sqcIndex = 0;
        for (int i = 0; i < strArray.length; i += 4) {

            if(strArray[i].equals("Fib")){
                sqc[sqcIndex] = new FibonacciSequence(Integer.parseInt(strArray[i+1]), Integer.parseInt(strArray[i+2]));
            } else if(strArray[i].equals("Sub")){
                sqc[sqcIndex] = new SubtractonacciSequence(Integer.parseInt(strArray[i+1]), Integer.parseInt(strArray[i+2]));
            }
            sqcIndex++;
        }
        return sqc;
        
    }
}
