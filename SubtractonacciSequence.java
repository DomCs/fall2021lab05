//Domenico Cuscuna 2038364

public class SubtractonacciSequence extends Sequence{
    private int numOne;
    private int numTwo;


    public SubtractonacciSequence(int numOne, int numTwo){
        this.numOne = numOne;
        this.numTwo = numTwo;
    }

    public int getTerm(int n){

        int[] numSubtr = new int[n+1];

        if(n < 0){
            throw new IllegalArgumentException("Value cannot be negative");
        }
        else if (n == 0){
            numSubtr[0] = numOne;
        }
        else{
            numSubtr[0] = numOne;
            numSubtr[1] = numTwo;
        }

        //Returns two first values
        if (n == 0 || n == 1){
            return numSubtr[n];
        }

        //If n is equal to 3 or more
     

        //Do data validation after production code
        for(int i = 2; i<numSubtr.length; i++){
            numSubtr[i] = numSubtr[i-2] - numSubtr[i-1];
        }

        return numSubtr[n];
    }
    
}
